function alertFun() {
    alert("¡Mensaje de Alerta!");
}

function confirmFun() {
    var mensaje; 

    if( confirm("Presiona el botón aceptar") ) {
        mensaje = "Gracias por presionar el botón";
    } else {
        mensaje = "Cancelaste la acción";
    }
    
    document.getElementById("actionAlert").innerHTML = mensaje;
}

function promptFun(){
    var segundoMensaje;
    var nombre = prompt("Ingresa tu nombre: ", "David");

    if ( nombre == null || nombre == "" ) {
        segundoMensaje = "¡No escribiste tu nombre! :(" 
    } else {
        segundoMensaje = "Tu nombre es " + nombre + "es feo.";
    }    

    document.getElementById("actionAlert").innerHTML = segundoMensaje;
    
}